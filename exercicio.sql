--1.1

CREATE OR REPLACE FUNCTION fn_consultar_saldo(codigo_cliente INTEGER, codigo_conta INTEGER)
RETURNS NUMERIC AS $$
DECLARE
    saldo NUMERIC;
BEGIN
    
    SELECT saldo INTO saldo
    FROM contas
    WHERE cliente_id = codigo_cliente AND conta_id = codigo_conta;
    
    
    RETURN saldo;
END;
$$ LANGUAGE plpgsql;


--1.2
CREATE OR REPLACE FUNCTION fn_transferir(
    codigo_cliente_remetente INTEGER, 
    codigo_conta_remetente INTEGER, 
    codigo_cliente_destinatario INTEGER, 
    codigo_conta_destinatario INTEGER, 
    valor_transferencia NUMERIC
)
RETURNS BOOLEAN AS $$
DECLARE
    saldo_remetente NUMERIC;
    saldo_destinatario NUMERIC;
BEGIN
    
    SELECT saldo INTO saldo_remetente
    FROM contas
    WHERE cliente_id = codigo_cliente_remetente AND conta_id = codigo_conta_remetente;

    SELECT saldo INTO saldo_destinatario
    FROM contas
    WHERE cliente_id = codigo_cliente_destinatario AND conta_id = codigo_conta_destinatario;

    
    IF saldo_remetente < valor_transferencia THEN
        RETURN FALSE; 
    END IF;


    UPDATE contas
    SET saldo = saldo - valor_transferencia
    WHERE cliente_id = codigo_cliente_remetente AND conta_id = codigo_conta_remetente;

    UPDATE contas
    SET saldo = saldo + valor_transferencia
    WHERE cliente_id = codigo_cliente_destinatario AND conta_id = codigo_conta_destinatario;

    RETURN TRUE; 
EXCEPTION
    WHEN OTHERS THEN
      
        RETURN FALSE;
END;
$$ LANGUAGE plpgsql;

--1.3